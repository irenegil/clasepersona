import unittest
from class_persona import Paciente,Medico, Persona

class TestPaciente(unittest.TestCase):
    def setUp(self):
        self.paciente = Paciente ("Irene","Gil","05/12/2005", "12645678P", "Dermatóloga")

    def test_ver_historial (self):
        self.assertEqual(self.paciente.ver_historial(),"Dermatóloga")


class TestMedico(unittest.TestCase):
    def setUp(self):
        self.medico = Medico("Andrea","Ospino","05/03/2002", "84937634R", "Cirujía","15 Octubre 2021 a las 15:00")

    def test_consultar_agenda (self):
        self.assertEqual(self.medico.consultar_agenda(),"15 Octubre 2021 a las 15:00, Cirujía")
                         

if __name__ == '__main__':
    unittest.main()