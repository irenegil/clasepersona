class Persona:
    def __init__ (self, n, a, f, d):
        self.nombre = n
        self.apellido = a
        self.fecha_nacimiento = f
        self.dni = d

    def __str__ (self):
        return 'El nombre completo es {nom} {ap}, la fecha de nacimiento es {fech} y su DNI es {dn}'. format(nom=self.nombre, ap=self.apellido, fech=self.fecha_nacimiento, dn= self.dni)

    def setNombre (self, n):
        self.nombre = n
    def getNombre (self):
        return self.nombre
    
    def setApellido (self, a):
        self.apellido = a
    def getApellido (self):
        return self.apellido
    
    def setFecha (self, f):
        self.fecha_nacimiento = f
    def getFecha (self):
        return self.fecha_nacimiento
    
    def setDNI (self, d):
        self.dni = d
    def getDNI (self):
        return self.dni
    
    

class Paciente (Persona):
    def __init__ (self, n, a, f, d, h):
        super (). __init__ (n, a, f, d)
        self.historial_clinico = h

    def ver_historial (self):
        return self.historial_clinico
    
class Medico (Persona):
    def __init__ (self, n, a, f, d, e, c):
        super (). __init__ (n, a, f, d)
        self.especialidad = e
        self.cita = c
    def consultar_agenda (self):
        return "{c}, {e}" .format(c=self.cita, e=self.especialidad)
   
paciente=Paciente("Irene","Gil","05/12/2005","12645678P","Dermatóloga")
medico= Medico("Andrea","Ospino","05/03/2002", "84937634R", "Cirujía","15 Octubre 2021 a las 15:00")


pacientes = [paciente, medico]
for people in pacientes:
    print (people)
    if people == paciente:
        print(people.ver_historial())
    elif people == medico:
        print(people.consultar_agenda())